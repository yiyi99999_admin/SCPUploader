package uploader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 导入和导出上传方案<br>
 * 当你在界面上选择了上传的文件,执行完上传后,没准要切换到另外一个上传文件集合. <br>
 * 这时你想保存当前的配置, 就可使用导出功能来备份当前上传文件列表和服务器信息到一个文件中. <br>
 * 之后当需要切换回来,就使可用导入功能显示之前的上传方案信息到界面上.
 *
 * @author zhangqunshi@126.com
 * @version java1.8, 2013-7-16
 */
public class Backup {

    private final Map<String, String> filePathMapping;
    private String hosts;
    private String username;
    private String password;

    public Backup() {
        filePathMapping = new HashMap<>();
    }

    public Map<String, String> getFilePathMapping() {
        return filePathMapping;
    }

    public String getHosts() {
        return hosts;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    /**
     * 导出当前的上传信息到一个文件中,作为备份
     *
     * @param file
     * @param fileMapping
     * @param hosts
     * @param username
     * @param password
     * @throws IOException
     */
    public static void exportConfig(File file, Map<String, String> fileMapping,
            String hosts, String username, char[] password)
            throws IOException {

        Iterator<String> it = fileMapping.keySet().iterator();
        StringBuilder sb = new StringBuilder();
        while (it.hasNext()) {
            String key = it.next();
            String value = fileMapping.get(key);
            sb.append(key).append(Utils.SEPARATOR).append(value).append("\n");
        }

        String lineSeparator = System.getProperty("line.separator");

        try (FileWriter fw = new FileWriter(file)) {
            fw.write("---path---" + lineSeparator);
            fw.write(sb.toString());
            fw.write(lineSeparator + "---hosts---" + lineSeparator);
            fw.write(hosts);
            fw.write(lineSeparator + "---user---" + lineSeparator);
            fw.write(username);
            fw.write(lineSeparator + "---pwd---" + lineSeparator);
            fw.write(password);
        }
    }

    /**
     * 导入备份文件
     *
     * @param file 备份文件
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void importConfig(File file)
            throws FileNotFoundException, IOException {

        StringBuilder sb;
        try (FileReader reader = new FileReader(file)) {
            char[] buff = new char[1024];
            sb = new StringBuilder();
            while (reader.read(buff) != -1) {
                sb.append(buff);
            }
        }

        String lineSeparator = System.getProperty("line.separator");
        String[] lines = sb.toString().split(lineSeparator);

        String flag = null;

        StringBuilder hostsb = new StringBuilder();
        StringBuilder usersb = new StringBuilder();
        StringBuilder pwdsb = new StringBuilder();

        for (int i = 0; i < lines.length; i++) {

            // 忽略空行
            lines[i] = lines[i].trim();
            if ("".equals(lines[i])) {
                continue;
            }

            switch (lines[i]) {
                case "---path---":
                    flag = "path";
                    continue;
                case "---hosts---":
                    flag = "hosts";
                    continue;
                case "---user---":
                    flag = "user";
                    continue;
                case "---pwd---":
                    flag = "pwd";
                    continue;
            }

            if ("path".equals(flag)) {

                if (!lines[i].contains(Utils.SEPARATOR)) {
                    System.out.println("Invalid format" + lines[i]);
                    continue;
                }

                String[] cols = lines[i].split(Utils.SEPARATOR);
                File f = new File(cols[0].trim());

                if (!f.exists()) {
                    System.out.println("Not found file: " + cols[0]);
                    continue;
                }

                this.filePathMapping.put(cols[0].trim(), cols[1].trim());

            }
            if ("hosts".equals(flag)) {
                hostsb.append(lines[i]);
            }
            if ("user".equals(flag)) {
                usersb.append(lines[i]);
            }
            if ("pwd".equals(flag)) {
                pwdsb.append(lines[i]);
            }
        }

        this.hosts = hostsb.toString().trim();
        this.username = usersb.toString().trim();
        this.password = pwdsb.toString().trim();

    }

}
