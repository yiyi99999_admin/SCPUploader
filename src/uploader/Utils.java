package uploader;

import java.io.File;
import java.util.Iterator;
import java.util.Map;

/**
 * 工具类
 *
 * @author Kris, zhangqunshi@126.com
 * @version java1.8, 2013-7-16
 */
public abstract class Utils {

    static final String SEPARATOR = " ==> ";

    /**
     * 把一个字典内容转成一个字符串, 显示在文件列表输入框内
     *
     * @param fileMapping
     * @return String of localFile and remote Path mapping
     */
    public static String format(Map<String, String> fileMapping) {
        StringBuilder sb = new StringBuilder();

        Iterator<String> it = fileMapping.keySet().iterator();

        while (it.hasNext()) {
            String key = it.next();
            String value = fileMapping.get(key);
            File tmp = new File(key);
            sb.append(tmp.getName()).append(": ").append(key)
                    .append(SEPARATOR).append(value).append("\n");
        }
        return sb.toString();
    }

}
